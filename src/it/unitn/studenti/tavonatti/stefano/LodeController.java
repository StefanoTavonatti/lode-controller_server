package it.unitn.studenti.tavonatti.stefano;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import it.unitn.studenti.tavonatti.stefano.interfaces.Publisher;
import it.unitn.studenti.tavonatti.stefano.interfaces.Subscriber;
import it.unitn.studenti.tavonatti.stefano.network.LodeControllerListner;
import it.unitn.studenti.tavonatti.stefano.network.fileManager.LodeTransferManager;
import it.unitn.studenti.tavonatti.stefano.utilities.Utilities;

public class LodeController implements Publisher {
	
	public final static int CONTROLLER_PORT=2147;
	public final static int TRASMISSION_PORT=2148;
	public static String sessionID="";
	private ServerSocket serverSocket;
	private LodeTransferManager lodeTransferManager=null;
	private ArrayList<Subscriber> subscribers;
	
	/**
	 * Se impostata a true il server gira in modalità debug
	 */
	public final static boolean DEBUG_MODE=true;
	
	public boolean cont=true;
	
	public LodeController(){
		
		subscribers=new ArrayList<Subscriber>();
		
		zeroConfiguration();
		sessionID();
		printMyAdresses();
		loadActiveSlide();
		startContinueThread();
		startFileManager();
		startListener();
	}
	
	public void startListener(){		
		//ServerSocket serverSocket=null;
		try {
			serverSocket = new ServerSocket(CONTROLLER_PORT);
			while(cont)
			{
				Socket socket=serverSocket.accept();
				
				Thread controllerThread=new Thread(new LodeControllerListner(socket,this));
				controllerThread.start();
			}
			
		} catch (IOException e) {
			// TODO Messaggio di errore//TODO cancellare stack trace
			//e.printStackTrace();
		} finally {
			if(serverSocket!=null){
				if(!serverSocket.isClosed()){
					
					/*try {
						serverSocket.close();
						Utilities.debugPrintln("Closing socket");
					} catch (IOException e) {
						e.printStackTrace();
					}*/
					
				}
			}
		}
		
	}
	
	public void startFileManager(){
		lodeTransferManager=new LodeTransferManager();
		Thread th=new Thread(lodeTransferManager);
		th.start();
	}
	
	public void startLisener2(){//TODO realn e tasto quit
		try {
			ServerSocket serverSocket=new ServerSocket(CONTROLLER_PORT);
			Socket socket=serverSocket.accept();
			
			BufferedReader in = new BufferedReader( new InputStreamReader(socket.getInputStream()));
			String inputLine="";
			
			PrintWriter out =
			        new PrintWriter(socket.getOutputStream(), true);
			
			 while ((inputLine = in.readLine()) != null) {
				  System.out.println(inputLine);
				  out.println("OK");
			  }
			
		} catch (IOException e) {
			// TODO Aggiungere messaggio di errore
			e.printStackTrace();
		}
	}
	
	/**
	 * stampa tutti gli indirri IP del pc
	 */
	private void printMyAdresses(){
		System.out.println("Session ID "+sessionID);
		
		try {
			/*recupero tutte le interfacce di rete del pc*/
			Enumeration<NetworkInterface> interfaces=NetworkInterface.getNetworkInterfaces(); //TODO controllare Enumeration
			
			while(interfaces.hasMoreElements()){
				
				NetworkInterface iface=interfaces.nextElement();
				
				if(!iface.isLoopback()){
					/*recupero tutti gli indirizzi attivi sull'interfaccia*/
					Enumeration<InetAddress> address=iface.getInetAddresses();
					
					while(address.hasMoreElements()){
						InetAddress add= address.nextElement();
						System.out.println(iface.getName()+" "+ add.getHostAddress()); //TODO controllare ipv6
					}
				}
			}
		} catch (SocketException e) {
			//TODO aggiungere messaggio di errore
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
		new LodeController();
		
	}
	
	/**
	 * crea la configurazione iniziale se non esiste
	 */
	private void zeroConfiguration(){
		File slideDir=Utilities.slideDir;
		File presetDir=Utilities.presetDir;
		
		if(!slideDir.exists()){
			if(!slideDir.mkdirs())
			{
				System.err.println("Unable to create slide directory");
				System.exit(1);
			}
			
		}
		
		if(!presetDir.exists())
		{
			if(!presetDir.mkdirs()){
				System.err.println("Unable to create preset directory");
				System.exit(1);
			}
		}
		
	}
	
	private void startContinueThread(){
	
		Thread th=new Thread(new Runnable() {
			
			@Override
			public void run() {
				BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
				String input="";
				
				do{
					try {
						input=br.readLine();
					} catch (IOException e) {
						input="";
						System.err.println("Keyboard Error");
					}
					
					if(input.equals("q")){
						cont=false;//TODO systemare
						System.out.println("GoodBye");
						
						try {
							serverSocket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						
						lodeTransferManager.closeSocket();
						
						System.exit(0);
					}else if(input.equals("s")){
						System.out.println("Status: "+LodeControllerListner.status);
					}else if(input.equals("as")){
						System.out.println("Active slide: "+LodeControllerListner.activeSlide);
					}else if(input.equals("ap")){
						System.out.println("Active preset: "+LodeControllerListner.activePreset);
					}
					//System.out.println(input);
				}while(!input.equals("q"));
				
			}
		});
		
		th.start();
	}
	
	private void sessionID(){
		String username=System.getProperty("user.name"); //platform independent 
		sessionID=username+"-"+System.currentTimeMillis();
	}
	
	private void loadActiveSlide(){

		ArrayList<File> slides=Utilities.getFileList(Utilities.slideDir);
		
		/*imposto la prima slide come slide attiva*/
		if(slides.size()>0)	
			LodeControllerListner.activeSlide=1;
		
		ArrayList<File> presets=Utilities.getFileList(Utilities.presetDir);
		/* imposto il primo preset come attivo*/
		if(presets.size()>0)
			LodeControllerListner.activePreset=1;
	}

	@Override
	public boolean subscribe(Subscriber subscriber) {
		synchronized (subscribers) {
			subscribers.add(subscriber);
		}
		
		return true;
	}

	@Override
	public boolean unSubscribe(Subscriber subscriber) {

		synchronized (subscribers) {
			subscribers.remove(subscriber);//TODO utilizza l'equals, controllare
		}

		return false;
	}

	@Override
	public void sendNotification(int notificationID) {//TODO controllare sincronizzazione
		synchronized (subscribers) {
			Iterator<Subscriber> iterator=subscribers.iterator();
			while(iterator.hasNext()){
				Subscriber sub=iterator.next();
				sub.onNotify(notificationID);
			}
		}
	}
}
