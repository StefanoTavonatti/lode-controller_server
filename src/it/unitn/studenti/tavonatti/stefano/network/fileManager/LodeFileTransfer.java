package it.unitn.studenti.tavonatti.stefano.network.fileManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

import it.unitn.studenti.tavonatti.stefano.utilities.NetworkMessage;
import it.unitn.studenti.tavonatti.stefano.utilities.Utilities;

public class LodeFileTransfer implements Runnable{//TODO chiusura server socket

	private Socket socket;
	
	public LodeFileTransfer(Socket socket) {
		this.socket=socket;
	}
	
	@Override
	public void run() {
		
		try {
			BufferedReader in = new BufferedReader( new InputStreamReader(socket.getInputStream()));
		
			String input=in.readLine();
			
			Hashtable<String, String> decodedMaessage=Utilities.decodeMeassage(input);
			
			String command=decodedMaessage.get(NetworkMessage.COMMAND_FIELD);
			String value=decodedMaessage.get(NetworkMessage.VALUES_FIELD);
			
			if(command==null || value==null){
				System.err.println("File Manager: Invalide command");
				return;
			}
			
			if(command.equals(NetworkMessage.GET_SLIDE)){
				
				sendSlide(value);
				
			}else if(command.equals(NetworkMessage.GET_PRESET)){
				
				sendPreset(value);
			}else {
				System.err.println("Unknown Command");
			}
			
			if(!socket.isClosed())
			{
				socket.close();
			}
			
			Utilities.debugPrintln("done");
			
		} catch (IOException e) {
			System.err.println("Connection Error");
		} catch (JSONException e) {
			System.err.println("Invalid Command");
			
			try {
				socket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		
		
	}
	
	private void sendSlide(String name){
		File slide=new File(Utilities.slideDir.getPath()+"/"+name);
		Utilities.debugPrintln("sending file: "+slide.getPath());
		transferFile(slide);
	}
	
	private void sendPreset(String name){
		File preset=new File(Utilities.presetDir.getPath()+"/"+name);
		Utilities.debugPrintln("sending file: "+preset.getPath());
		transferFile(preset);
	}
	
	private void transferFile(File file){
		
		if(!file.exists()){
			try {
				System.err.println("File not found");
				PrintWriter outWriter=new PrintWriter(socket.getOutputStream(),true);
				String output=Utilities.getMessage(NetworkMessage.FILE_NOT_FOUND, "").toString();
				
				outWriter.println(output);
				outWriter.close();
				return;
			} catch (IOException e) {
				System.err.println("Trasmission error");
				return;
			}
		}
		else {
			PrintWriter outWriter;
			try {
				outWriter = new PrintWriter(socket.getOutputStream(),true);
				String output=Utilities.getMessage(NetworkMessage.ACKNOWLEDGE, "").toString();
				
				outWriter.println(output);
			} catch (IOException e) {
				System.err.println("Trasmission error");
				return;
			}
		}
		
		byte[] bytes = new byte[16 * 1024];
		try {
			
			InputStream in = new FileInputStream(file);
			OutputStream out=socket.getOutputStream();
			
			int count;
	        while ((count = in.read(bytes)) > 0) {
	            out.write(bytes, 0, count);
	        }
	        
	        out.close();
	        in.close();
			
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
