package it.unitn.studenti.tavonatti.stefano.network.fileManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import it.unitn.studenti.tavonatti.stefano.LodeController;

public class LodeTransferManager implements Runnable{
	
	private ServerSocket serverSocket=null;

	public LodeTransferManager() {
		super();
	}
	
	@Override
	public void run() {
		
		try {
			serverSocket=new ServerSocket(LodeController.TRASMISSION_PORT);
			
			while(true){//TODO static cont in lode controller
				Socket client=serverSocket.accept();
				
				Thread th=new Thread(new LodeFileTransfer(client));
				th.start();
			}
			
			//serverSocket.close();//TODO sistemare
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeSocket(){
		if(serverSocket!=null){
			if(!serverSocket.isClosed()){
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
