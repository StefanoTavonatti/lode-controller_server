package it.unitn.studenti.tavonatti.stefano.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import it.unitn.studenti.tavonatti.stefano.LodeController;
import it.unitn.studenti.tavonatti.stefano.interfaces.Publisher;
import it.unitn.studenti.tavonatti.stefano.interfaces.Subscriber;
import it.unitn.studenti.tavonatti.stefano.utilities.NetworkMessage;
import it.unitn.studenti.tavonatti.stefano.utilities.Utilities;

public class LodeControllerListner implements Runnable,Subscriber{

	private Socket clientSocket;
	public static String status=NetworkMessage.STOP;//statica: visibile da ovunque e unica
	public static int activeSlide=0;
	public static int activePreset=0;
	private ArrayList<File> slide;
	private ArrayList<File> preset;
	private Publisher publisher;
	private PrintWriter outGlobal;
	
	/**
	 * contiene true, se il client è verificato, se è false il client non piò eseguire operazioni
	 */
	private boolean verifiedClient=false;
	
	/**
	 * pin per verificare la connessione
	 */
	private int pin;
	
	public LodeControllerListner(Socket clientSocket, Publisher publisher) {
		this.clientSocket=clientSocket;
		pin=Utilities.genPin(Utilities.MAX_PIN);
		slide=Utilities.getFileList(Utilities.slideDir);
		preset=Utilities.getFileList(Utilities.presetDir);
		this.publisher=publisher;
		publisher.subscribe(this);
				
	}
	
	@Override
	public void run() {
		int port=clientSocket.getPort();
		try {
			System.out.println("Opening connection with client "+clientSocket.getInetAddress()+" on port "+port+" -- authorization code "+ pin);
			BufferedReader in = new BufferedReader( new InputStreamReader(clientSocket.getInputStream()));
			String input="";
			
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			outGlobal=out;
            
			
			while((input=in.readLine())!=null){
				
				Hashtable<String, String> decodedMessage=Utilities.decodeMeassage(input);
				
				String comm=decodedMessage.get(NetworkMessage.COMMAND_FIELD);
				String value=decodedMessage.get(NetworkMessage.VALUES_FIELD);
				
				if(comm==null){
					System.err.println("Invalid command");
					continue;
				}
				
				if(!verifiedClient){
					if(!comm.equals(NetworkMessage.PIN)){
						out.println(Utilities.getMessage(NetworkMessage.UNATHORIZED, "").toString());	
						continue;
					}
					else
					{
						if(value!=null){
							try
							{
								int tempPin=Integer.parseInt(value);
								verifiedClient= pin==tempPin;
							}
							catch (NumberFormatException e){
								System.err.println("Invalid pin");
							}
						}
						
						String response="";
						
						if(verifiedClient){
							response=Utilities.getMessage(NetworkMessage.AUTHORIZED,"").toString();
						}
						else {
							response=Utilities.getMessage(NetworkMessage.UNATHORIZED, "").toString();
						}
						
						out.println(response);
						
					}
				}
				
				Utilities.debugPrintln(comm);
				
				
				switch(comm)
				{
					case NetworkMessage.GET_SLIDE_LIST:
						sendSlideList(out);
						break;
						
					case NetworkMessage.GET_PRESET_LIST:
						sendPresetList(out);
						break;
						
					case NetworkMessage.GET_SESSION_ID:
						sendSessionID(out);
						break;
						
					case NetworkMessage.STOP:
						setStatus(out, NetworkMessage.STOP);
						break;
						
					case NetworkMessage.START:
						setStatus(out, NetworkMessage.START);
						break;
						
					case NetworkMessage.PAUSE:
						setStatus(out, NetworkMessage.PAUSE);
						break;
					
					case NetworkMessage.GET_STATUS:
						sendStatus(out);
						break;
					case NetworkMessage.GET_ACTIVE_SLIDE:
						sendActiveSlide(out);
						break;
					case NetworkMessage.GET_ACTIVE_PRESET:
						sendActivePreset(out);
						break;
					case NetworkMessage.NEXT:
						nextSlide(out, slide);
						notifySlide();
						break;
					case NetworkMessage.PREVIOUS:
						previousSlide(out, slide);
						notifySlide();
						break;
					case NetworkMessage.SET_SLIDE:
						setSlide(value);
						notifySlide();
						break;
					case NetworkMessage.SET_PRESET:
						setPreset(value);
						notifyPreset();
						break;
					case NetworkMessage.PIN:
						/* aggiunto per evitare la stampa di "Invalid command" dopo un login eseguito correttamente*/
						break;
					default:
						System.err.println("Invalid command");
						break;
				}
				//TODO aggiungere comando non valido anche se il formato è giusto
			}
			
			System.out.println("Closing connection with client "+clientSocket.getInetAddress()+" on port "+port);
			publisher.unSubscribe(this);
			clientSocket.close();
			
		} catch (IOException e) {
			// TODO Messaggio Errore
			e.printStackTrace();
		}
		
		
	}
	
	private void notifySlide(){
		publisher.sendNotification(Utilities.SLIDE_UPDATE);
	}
	
	private void notifyPreset(){
		publisher.sendNotification(Utilities.PRESET_UPDATE);
	}
	
	private void notifyStatus(){
		publisher.sendNotification(Utilities.STATUS_UPDATE);
	}
	
	private void sendSlideList(PrintWriter out){
		ArrayList<File> files=Utilities.getFileList(Utilities.slideDir);

		String output=Utilities.getMessage(NetworkMessage.SLIDES, ""+files.size());
		out.println(output);
		
	}
	
	private void sendPresetList(PrintWriter out){
		ArrayList<File> files=Utilities.getFileList(Utilities.presetDir);
		
		String output=Utilities.getMessage(NetworkMessage.PRESETS, ""+files.size());
		out.println(output);
	}
	
	private void sendSessionID(PrintWriter out){
		String output=Utilities.getMessage(NetworkMessage.SESSION_ID, LodeController.sessionID).toString();
		out.println(output);
	}
	
	private void setStatus(PrintWriter out,String newStatus){
		status=newStatus;
		//sendStatus(out);
		notifyStatus();
	}
	
	private void sendStatus(PrintWriter out){
		String output=Utilities.getMessage(NetworkMessage.STATUS, status).toString();
		out.println(output);
	}
	
	private void sendActiveSlide(PrintWriter out){
		String output=Utilities.getMessage(NetworkMessage.ACTIVE_SLIDE, ""+activeSlide).toString();//TODO inviare un intero
		out.println(output);
	}
	
	private void sendActivePreset(PrintWriter out){
		String output=Utilities.getMessage(NetworkMessage.ACTIVE_PRESET, ""+activePreset).toString();
		out.println(output);
	}
	
	private void previousSlide(PrintWriter out,ArrayList<File> files){
		if(activeSlide>0){
			activeSlide--;
		}
	}
	
	private void nextSlide(PrintWriter out,ArrayList<File> files){
		int numeFiles=files.size();
		if(activeSlide<numeFiles){
			activeSlide++;
		}
	}
	
	private void setSlide(String newSlide){
		
		try{
			activeSlide=Integer.valueOf(newSlide).intValue();
		}
		catch(NumberFormatException e){
			System.out.println("Unknown slide");
		}
	}
	
	private void setPreset(String newPreset){
		try{
			activePreset=Integer.valueOf(newPreset).intValue();
		}
		catch(NumberFormatException e){
			System.out.println("Unknown preset");
		}
	}

	@Override
	public void onNotify(int notifyID) {
		// TODO controllare codice notifica e inviare aggiornamento al client android
		//TODO non posso creare un altro print writer
		
		switch(notifyID){
			case Utilities.SLIDE_UPDATE:
				sendActiveSlide(outGlobal);
				break;
			case  Utilities.PRESET_UPDATE:
				sendActivePreset(outGlobal);
				break;
			case Utilities.STATUS_UPDATE:
				sendStatus(outGlobal);
				break;
		}
	}
	
	public String getAddress(){
		return clientSocket.getInetAddress().getHostName();
	}
	
	@Override
	public boolean equals(Object obj){
		
		if(!(obj instanceof LodeControllerListner))
			return false;
		
		LodeControllerListner temp=(LodeControllerListner) obj;
		
		return temp.getAddress().equals(this.getAddress());
	}	
	

}
