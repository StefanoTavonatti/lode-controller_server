package it.unitn.studenti.tavonatti.stefano.interfaces;

public interface Publisher {

	/**
	 * registrazione di un nuovo subscriber
	 * @param subscriber
	 * @return 
	 */
	public boolean subscribe(Subscriber subscriber);
	
	/**
	 * eliminazione di un subscriber
	 */
	public boolean unSubscribe(Subscriber subscriber);
	
	/**
	 * send notification to all subscriber
	 */
	public void sendNotification(int notificationID);
}
