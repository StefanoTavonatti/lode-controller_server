package it.unitn.studenti.tavonatti.stefano.interfaces;

public interface Subscriber {
	
	public void onNotify(int notifyID);

}
