package it.unitn.studenti.tavonatti.stefano.utilities;

public class NetworkMessage {

	/**
	 * Campo command dei JSON di comunicazione
	 */
	public final static String COMMAND_FIELD="command";
	
	/**
	 * Campo values dei JSON di comunicazione
	 */
	public final static String VALUES_FIELD="values";
	
	/**
	 * notifica di avvenuta trasmissione
	 */
	public final static String ACKNOWLEDGE="ACK";
	
	/**
	 * indica che il messaggio contiene la lista delle slides
	 */
	public final static String SLIDES="SLIDES";
	
	/**
	 * indica che il messaggio contiene la lista dei preset
	 */
	public final static String PRESETS="PRESETS";
	
	/**
	 *  richiede la trasmissione dell'elenco delle slide
	 */
	public final static String GET_SLIDE_LIST="GETNSLIDES";
	
	/**
	 * richiede la trasmissione della lista dei preset
	 */
	public final static String GET_PRESET_LIST="GETNPRESETS";
	
	/**
	 * notifica la fine dell'elenco delle slide
	 */
	public final static String END_OF_SLIDE="EOS";
	
	/**
	 * notifica la fine dell'elenco dei preset	
	 */
	public final static String END_OF_PRESET="EOP";
	
	public final static String START="RECORDING";
	
	public final static String STOP="STOP";
	
	public final static String PAUSE="PAUSE";
	
	public final static String NEXT="NEXT";
	
	public final static String PREVIOUS="PREVIOUS";
	
	/**
	 * indica al server di settare la slide num n
	 */
	public final static String SET_SLIDE="SHOW";
	
	/**
	 * indica al server di settare il preset num n
	 */
	public final static String SET_PRESET="PRESET";
	
	/**
	 * messaggio per richiedere il session id
	 */
	public final static String GET_SESSION_ID="GET_SESSION_ID";
	
	/**
	 * specifica che il messaggio contiene il session id
	 */
	public final static String SESSION_ID="SESSION_ID";
	
	/**
	 * specifica che il messaggio contiene lo status
	 */
	public final static String STATUS="STATUS";
	
	/**
	 * richiede lo stato attuale
	 */
	public final static String GET_STATUS="GET_STATUS";
	
	/**
	 * richiede il trasferimento di una precisa slide
	 */
	public final static String GET_SLIDE="GET_SLIDE";
	
	/**
	 * richiede il trasferimento di uno specifico preset
	 */
	public final static String GET_PRESET="GET_PRESET";
	
	/**
	 * indica che il file richiesto non esiste
	 */
	public final static String FILE_NOT_FOUND="FILE_NOT_FOUND";
	
	/**
	 * richiede il nome della slide attualmente attiva
	 */
	public final static String GET_ACTIVE_SLIDE="GET_ACTIVE_SLIDE";
	
	/**
	 * richiede il nome del preset attualmente attivo
	 */
	public final static String GET_ACTIVE_PRESET="GET_ACTIVE_PRESET";
	
	/**
	 * indica che il messaggio contiene il nome della slide attiva
	 */
	public final static String ACTIVE_SLIDE="ACTIVE_SLIDE";
	
	/**
	 * indica che il messaggio contiene il nome del preset attivo
	 */
	public final static String ACTIVE_PRESET="ACTIVE_PRESET";
	
	/**
	 * indica che il messaggio contiene il pin di verifica del client
	 */
	public final static String PIN="LOGIN";
	
	/**
	 * indica che il client non è autorizzato
	 */
	public final static String UNATHORIZED="UNAUTHORIZED";
	
	/**
	 * indica che il client è autorizzato
	 */
	public final static String AUTHORIZED="AUTHORIZED";
	
	/**
	 * 
	 */
	public final static String FIRST="FIRST";
	
	/**
	 * 
	 */
	public final static String LAST="LAST";
}
