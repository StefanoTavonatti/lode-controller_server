package it.unitn.studenti.tavonatti.stefano.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import it.unitn.studenti.tavonatti.stefano.LodeController;

public class Utilities {

	public final static File slideDir=new File("slide");
	public final static File presetDir=new File("preset");
	public final static int SLIDE_UPDATE=1;
	public final static int PRESET_UPDATE=2;
	public final static int STATUS_UPDATE=3;
	public final static int MAX_PIN=9999;
	
	/**
	 * restituisce un array list contenente l'elenco dei file presenti in una cartella, escludendo le sottocartelle
	 * @param directory la directory di cui si vuole ottenere l'elenco dei file
	 * @return ArrayList contenete i file presenti nella cartella
	 */
	public static ArrayList<File> getFileList(File directory){
		ArrayList<File> result=new ArrayList<File>();
		
		/*controllo l'esistenza della directory*/
		if(!directory.exists()){
			return result;
		}
		
		/*Controllo che la directory sia efettivamente una direcoty*/
		if(!directory.isDirectory()){
			return result;
		}
		
		File[] files=directory.listFiles();
		
		for(File file:files){
			
			/*prendo solo i file escludendo le cartelle*/
			if(file.isFile())
			{
				result.add(file);
			}
			
		}
		
		//TODO chiedere su come devo ordinare le slide
		/*result.sort(new Comparator<File>() {

			@Override
			public int compare(File file1, File file2) {
				String name1=file1.getName();
				String name2=file2.getName();
				
				return name1.compareToIgnoreCase(name2);
			}
		});*/
		
		result.sort(new Comparator<File>() {

			@Override
			public int compare(File file1, File file2) {
				String name1=file1.getName();
				String name2=file2.getName();
				
				name1=name1.split("\\.")[0];
				name2=name2.split("\\.")[0];
				
				return Integer.valueOf(name1).compareTo(Integer.valueOf(name2));
			}
		});
		
		return result;
	}
	
	public static JSONObject getJSONFileList(File directory,String type){
		
		ArrayList<File> files=getFileList(directory);
		ArrayList<String> filesString=new ArrayList<String>(files.size());
		
		Iterator<File> i=files.iterator();
		
		while(i.hasNext()){
			File file=i.next();
			
			filesString.add(file.getName());
		}
		
		JSONArray resultArray=new JSONArray(filesString.toArray());
		
		JSONObject result=new JSONObject();
		
		result.put(NetworkMessage.COMMAND_FIELD, type);
		result.put(NetworkMessage.VALUES_FIELD, resultArray);
		
		return result;
		
	}
	
	
	/**
	 * se il server è in modalità debug stampa la il contenuto della stringa output,
	 * altrimenti non stampa niente
	 * @param output la stringa da stampare
	 */
	public static void debugPrintln(String output){
		if(LodeController.DEBUG_MODE){
			System.out.println(output);
		}
	}
	
	public static String getMessage(String command,String value){
		String result="";
		
		result+=command;
		
		result+=" "+value;
		
		
		return result;
	}
	
	/**
	 * genere un pin casuale per verificare la connessione del clien
	 * @return
	 */
	public static int genPin(int maxValue){
	
		Random random=new Random();
		
		return random.nextInt(maxValue);
	}
	
	public static Hashtable<String, String> decodeMeassage(String message){
		Hashtable<String, String> result=new Hashtable<String,String>();
		
		String[] splits=message.split(" ");
		
		if(splits.length>=1){
			result.put(NetworkMessage.COMMAND_FIELD, splits[0]);
			
			if(splits.length>=2){
				result.put(NetworkMessage.VALUES_FIELD, splits[1]);
			}
		}
		
		return result;
	}
}
